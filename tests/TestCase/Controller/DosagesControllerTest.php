<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Api;

use App\Test\TestCase\Controller\AuthenticationTestTrait;
use App\Test\TestCase\Controller\CrudTestTrait;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\DosagesController Test Case
 *
 * @uses DosagesController
 */
class DosagesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use AuthenticationTestTrait;
    use CrudTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Dosages',
        'app.Products',
        'app.Cultures',
        'app.Pests',
        'app.Users',
    ];

    public function testGetDosageByProduct()
    {
        $this->login();
        $this->get($this->getEndpoint() . '?product_id=1&noPage=true');
        $this->assertCount(
            1,
            $this->transformResponse()
        );
    }

    public function testGetDosageByPest()
    {
        $this->login();
        $this->get($this->getEndpoint() . '?pest_id=1&noPage=true');
        $this->assertCount(
            1,
            $this->transformResponse()
        );
    }

    /**
     * @covers \App\Controller\DosagesController
     */
    public function testGetDosageByCulture()
    {
        $this->login();
        $this->get($this->getEndpoint() . '?culture_id=1&noPage=true');
        $this->assertCount(
            1,
            $this->transformResponse()
        );
    }

    /**
     * @covers \App\Controller\DosagesController
     */
    public function testListDosages()
    {
        $this->login();
        $this->getAllEntities();
        $this->assertResponseOk();
    }

    public function testViewDosage()
    {
        $idDosage = 1;
        $this->login();
        $this->getEntity($idDosage);
        $this->assertResponseOk();
    }

    public function testNotFoundDosage()
    {
        $this->login();
        $this->getEntity(4);
        $this->assertResponseCode(404);
    }

    public function testEditDosage()
    {
        $idDosage = 1;
        $this->login();
        $this->editEntity($idDosage, [
            'value' => 3,
        ]);
        $this->assertResponseOk();
    }

    public function testAddDosage()
    {
        $data = [
            'value' => 1,
            'unit_type' => 'L',
            'product_id' => 1,
            'culture_id' => 1,
            'pest_id' => 1,
        ];
        $this->login();
        $this->addEntity($data);
        $this->assertResponseOk();
    }

    public function testDeleteDosage()
    {
        $idDosage = 1;
        $this->login();
        $this->deleteEntity($idDosage);
        $this->assertResponseOk();
    }

    protected function getEndpoint(): string
    {
        return '/api/dosages';
    }
}
