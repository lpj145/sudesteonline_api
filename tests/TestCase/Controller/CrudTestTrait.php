<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

trait CrudTestTrait
{
    protected function deleteEntity($idEntity)
    {
        $this->delete($this->getUrl("/{$idEntity}"));

        return $this->transformResponse();
    }

    protected function editEntity($idEntity, $data)
    {
        $this->put($this->getUrl("/{$idEntity}"), $data);

        return $this->transformResponse();
    }

    protected function addEntity($data)
    {
        $this->post($this->getUrl(), $data);

        return $this->transformResponse();
    }

    protected function getEntity($idEntity)
    {
        $this->get($this->getUrl("/{$idEntity}"));

        return $this->transformResponse();
    }

    protected function getAllEntities()
    {
        $this->get($this->getUrl());

        return $this->transformResponse();
    }

    private function getUrl($append = '')
    {
        return $this->getEndpoint() . $append;
    }

    private function transformResponse(): ?array
    {
        return json_decode((string)$this->_response->getBody(), true);
    }

    abstract protected function getEndpoint(): string;
}
