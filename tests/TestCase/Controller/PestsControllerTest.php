<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Api;

use App\Test\TestCase\Controller\AuthenticationTestTrait;
use App\Test\TestCase\Controller\CrudTestTrait;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Api\PestsController Test Case
 *
 * @uses PestsController
 */
class PestsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use CrudTestTrait;
    use AuthenticationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Pests',
        'app.Users',
    ];

    public function testListPests()
    {
        $this->login();
        $this->getAllEntities();
        $this->assertResponseOk();
    }

    public function testViewPests()
    {
        $idPest = 1;
        $this->login();
        $this->getEntity($idPest);
        $this->assertResponseOk();
    }

    public function testNotFoundPest()
    {
        $this->login();
        $this->getEntity(4);
        $this->assertResponseCode(404);
    }

    public function testEditPest()
    {
        $idPest = 1;
        $this->login();
        $this->editEntity($idPest, [
            'name' => 'TestPestEditAction',
        ]);
        $this->assertResponseOk();
    }

    public function testAddPest()
    {
        $data = [
            'name' => 'PestTest',
        ];
        $this->login();
        $this->addEntity($data);
        $this->assertResponseOk();
    }

    public function testDeletePest()
    {
        $idPest = 1;
        $this->login();
        $this->deleteEntity($idPest);
        $this->assertResponseOk();
    }

    protected function getEndpoint(): string
    {
        return '/api/pests';
    }
}
