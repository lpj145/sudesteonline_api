<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Api;

use App\Test\TestCase\Controller\AuthenticationTestTrait;
use App\Test\TestCase\Controller\CrudTestTrait;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Api\CulturesController Test Case
 *
 * @uses CulturesController
 */
class CulturesControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use AuthenticationTestTrait;
    use CrudTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Cultures',
        'app.Users',
    ];

    public function testListCulture()
    {
        $this->login();
        $this->getAllEntities();
        $this->assertResponseOk();
    }

    public function testViewCulture()
    {
        $idCulture = 1;
        $this->login();
        $this->getEntity($idCulture);
        $this->assertResponseOk();
    }

    public function testNotFoundCulture()
    {
        $this->login();
        $this->getEntity(4);
        $this->assertResponseCode(404);
    }

    public function testEditCulture()
    {
        $idCulture = 1;
        $this->login();
        $this->editEntity($idCulture, [
            'name' => 'TestCultureEditAction',
        ]);
        $this->assertResponseOk();
    }

    public function testAddCulture()
    {
        $data = [
            'name' => 'CultureTest',
        ];
        $this->login();
        $this->addEntity($data);
        $this->assertResponseOk();
    }

    public function testDeleteProduct()
    {
        $idProduct = 1;
        $this->login();
        $this->deleteEntity($idProduct);
        $this->assertResponseOk();
    }

    protected function getEndpoint(): string
    {
        return '/api/cultures';
    }
}
