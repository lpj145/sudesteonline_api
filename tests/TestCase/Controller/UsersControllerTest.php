<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\UsersController Test Case
 *
 * @uses UsersController
 */
class UsersControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use AuthenticationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Users',
    ];

    public function testAuthenticateLogin()
    {
        $this->login();
        $this->assertResponseCode(200);
    }

    public function testUnauthenticated()
    {
        $this->post('/token');
        $this->assertResponseCode(401);
    }
}
