<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Api;

use App\Test\TestCase\Controller\AuthenticationTestTrait;
use App\Test\TestCase\Controller\CrudTestTrait;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Api\ProductsController Test Case
 *
 * @uses \App\Controller\Api\ProductsController
 */
class ProductsControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use AuthenticationTestTrait;
    use CrudTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Products',
        'app.Users',
    ];

    public function testListProducts()
    {
        $this->login();
        $this->getAllEntities();
        $this->assertResponseOk();
    }

    public function testViewProduct()
    {
        $idProduct = 5;
        $this->login();
        $this->getEntity($idProduct);
        $this->assertResponseOk();
    }

    public function testNotFoundProduct()
    {
        $this->login();
        $this->getEntity(4);
        $this->assertResponseCode(404);
    }

    public function testEditProduct()
    {
        $idProduct = 5;
        $this->login();
        $this->editEntity($idProduct, [
            'name' => 'TestProductEditAction',
        ]);
        $this->assertResponseOk();
    }

    public function testAddProduct()
    {
        $data = [
            'name' => 'ProductTest',
            'type' => 'Pest',
            'vendor_name' => 'agramate',
        ];
        $this->login();
        $this->addEntity($data);
        $this->assertResponseOk();
    }

    public function testDeleteProduct()
    {
        $idProduct = 5;
        $this->login();
        $this->deleteEntity($idProduct);
        $this->assertResponseOk();
    }

    protected function getEndpoint(): string
    {
        return '/api/products';
    }
}
