<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

/**
 * Trait AuthenticationTrait
 *
 * @package App\Test\TestCase\Controller
 * @property ResponseInterface $_response
 */
trait AuthenticationTestTrait
{
    protected $token = '';

    protected function authenticate()
    {
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);
        $this->post('/token', [
            'username' => 'test@email.com',
            'password' => 'testpass',
        ]);
        $data = json_decode((string)$this->_response->getBody(), true);
        if (!array_key_exists('token', $data)) {
            throw new \InvalidArgumentException('Cannot get token from login action.');
        }
        $this->token = $data['token'];

        return $this->token;
    }

    protected function login()
    {
        if (empty($this->token)) {
            $this->authenticate();
        }
        $this->configRequest([
            'headers' => [
                'Authorization' => 'Bearer ' . $this->token,
            ],
        ]);
    }

    protected function logout()
    {
        $this->configRequest([
            'headers' => [],
        ]);
    }
}
