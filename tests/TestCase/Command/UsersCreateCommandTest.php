<?php
declare(strict_types=1);

namespace App\Test\TestCase\Command;

use Cake\Command\Command;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Command\UsersCreateCommand Test Case
 *
 * @uses \App\Command\UsersCreateCommand
 */
class UsersCreateCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
    }

    /**
     * Test buildOptionParser method
     *
     * @return void
     */
    public function testBuildOptionParser(): void
    {
        $this->exec('users_create -h');
        $this->assertOutputContains('Add user to authentication with email/password and jwt token.');
    }

    /**
     * Test execute method
     *
     * @return void
     */
    public function testExecute(): void
    {
        $this->exec('users_create TestUser UserTest test@emailtest.com testpassword');
        $this->assertOutputContains('Welcome test@emailtest.com to sudeste api system.');
        $this->assertExitCode(Command::CODE_SUCCESS);
    }

    public function getFixtures(): array
    {
        return [
            'app.Users',
        ];
    }
}
