<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CulturesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CulturesTable Test Case
 */
class CulturesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CulturesTable
     */
    protected $Cultures;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Cultures',
        'app.Dosages',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Cultures') ? [] : ['className' => CulturesTable::class];
        $this->Cultures = TableRegistry::getTableLocator()->get('Cultures', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Cultures);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->assertTrue(true, 'Não é caso complexo.');
    }
}
