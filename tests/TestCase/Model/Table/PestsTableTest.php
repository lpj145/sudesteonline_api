<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PestsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PestsTable Test Case
 */
class PestsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PestsTable
     */
    protected $Pests;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Pests',
        'app.Dosages',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Pests') ? [] : ['className' => PestsTable::class];
        $this->Pests = TableRegistry::getTableLocator()->get('Pests', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Pests);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->assertTrue(true, 'Não é caso complexo.');
    }
}
