<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DosagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DosagesTable Test Case
 */
class DosagesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DosagesTable
     */
    protected $Dosages;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Dosages',
        'app.Products',
        'app.Cultures',
        'app.Pests',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Dosages') ? [] : ['className' => DosagesTable::class];
        $this->Dosages = TableRegistry::getTableLocator()->get('Dosages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Dosages);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->assertTrue(true, 'Não é caso complexo.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->assertTrue(true, 'Não é caso complexo.');
    }
}
