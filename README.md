### Sudeste Test
Este é o meu teste para o pedido de vocês, a versão utilizada do cakephp foi a 4,
alguns conceitos como automatização do swagger bem como consultas api foram amplamentes implementadas e testas por mim
em outras 30 aplicações que já criei utilizando cakephp, é um framework muito maduro, geralmente estou discutindo muitas
técnicas no slack junto da galera, obrigado pela atenção.

#### Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md)
If Composer is installed globally, run:

```bash
git clone https://gitlab.com/lpj145/sudesteonline_api
cd sudesteonline_api
composer install
```

### Environment
Create ``.env`` file on root application for containers.
````dotenv
APP_NAME=sudeste_api
DEBUG=true

DATABASE_USER=<-DATABASE_MYSQL_USER->
DATABASE_PASSWORD=<-DATABASE_MYSQL_PASSWORD->

DATABASE_URL=mysql://USER:PASS@localhost/sudeste_api
DATABASE_TEST_URL=mysql://USER:PASS@localhost/sudeste_test

````

### Docker
Before run command above, create environment file, see: environment section.

This docker structure have three containers: `mysql`, `phpmyadmin`, `apache+php`.
````bash
docker-composer up -d
````

Check ``http://localhost:8000`` for api host, ``http://localhost:8000/docs`` for swagger 3 docs.

### Docs
A good part of swagger is generated automatic, but you can see ``swagger.json`` file in root path.

### Rest Tests
For test i use [insomnia](https://insomnia.rest/download/) software.
``insomnia_test_api.json`` contain all tests for api, feel free to import and use.

### Seed
I create a seed called ``UserSeed`` run`bin/cake migrations seed`, a user called John Doe is created
and you can login with `test@email.com` and password `testmail`.

### Command
Have one command: ``users_create *name *last_name *username *password`` to create a user/api.

### Case Pdf Endpoint
The pdf endpoint is simple than doc want, i try to not complex without needed,
is clearly described in docs, and you can access it on: ``/api/dosages/pdf``.

### Tests
Remember, to run tests and stan, you need to install dev packages.

````bash
composer install --dev
composer run test
composer run stan
````

Obrigado pelo carinho e por sua atenção.
