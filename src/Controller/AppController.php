<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Controller\Controller;
use Cake\Event\EventInterface;
use Cake\Http\Response;
use ExpressRequest\Controller\Component\ExpressRequestComponent;

/**
 * Class AppController
 * @package App\Controller
 * @property AuthenticationComponent $Authentication
 * @property ExpressRequestComponent $ExpressRequest
 */
class AppController extends Controller
{
    /**
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Authentication.Authentication');
        $this->loadComponent('ExpressRequest.ExpressRequest');

        // Actually is a hack, is some months is still solved.
        if ($this->request->getParam('plugin') === 'SwaggerBake') {
            $this->Authentication->allowUnauthenticated(['index']);
        }
    }


    /**
     * @param $data
     * @param int $status
     * @return Response
     */
    protected function responseJson($data, int $status = 200)
    {
        return $this->response
            ->withStatus($status)
            ->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    protected function responseWithoutContent()
    {
        return (new Response())
            ->withStatus(204);
    }
}
