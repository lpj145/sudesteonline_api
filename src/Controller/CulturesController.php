<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Table\CulturesTable;
use SwaggerBake\Lib\Annotation\SwagQuery;
use SwaggerBake\Lib\Annotation\SwagResponseSchema;

/**
 * Cultures Controller
 * @property CulturesTable $Cultures
 */
class CulturesController extends AppController
{
    /**
     * @SwagQuery(name="name", description="find products by name.")
     * @SwagQuery(name="noPage", description="paginate data.", type="boolean")
     * @SwagResponseSchema(httpCode=200,refEntity="#/components/schemas/Pagination")
     * @return \Cake\Http\Response
     * @throws \ErrorException
     */
    public function index()
    {
        return $this->responseJson(
            $this->ExpressRequest->search(
                $this->request,
                $this->Cultures
            )
        );
    }

    public function view($id)
    {
        return $this->responseJson(
            $this->Cultures->getByIdOrFail($id)
        );
    }

    /**
     * @return \Cake\Http\Response
     */
    public function add()
    {
        $entity = $this->Cultures->instanceAndValidate(
            $this->request->getData()
        );

        $this->Cultures->saveOrFail($entity);
        return $this->responseJson($entity);
    }

    public function edit($id)
    {
        $this->Cultures->updateOrFail($id, $this->request->getData());
        return $this->responseWithoutContent();
    }

    public function delete($id)
    {
        $this->Cultures->deleteOrFailResponse($id);
        return $this->responseWithoutContent();
    }
}
