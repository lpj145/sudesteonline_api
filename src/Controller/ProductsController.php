<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Table\ProductsTable;
use SwaggerBake\Lib\Annotation\SwagQuery;
use SwaggerBake\Lib\Annotation\SwagResponseSchema;

/**
 * Products Controller
 * @property ProductsTable $Products
 */
class ProductsController extends AppController
{

    /**
     * @SwagQuery(name="name", description="find products by name.")
     * @SwagQuery(name="noPage", description="paginate data.", type="boolean")
     * @SwagResponseSchema(httpCode=200,refEntity="#/components/schemas/Pagination")
     * @return \Cake\Http\Response
     * @throws \ErrorException
     */
    public function index()
    {
        return $this->responseJson(
            $this->ExpressRequest->search(
                $this->request,
                $this->Products
            )
        );
    }

    public function view($id)
    {
        return $this->responseJson(
            $this->Products->getByIdOrFail($id)
        );
    }

    public function add()
    {
        $entity = $this->Products->instanceAndValidate(
            $this->request->getData()
        );

        $this->Products->saveOrFail($entity);
        return $this->responseJson($entity);
    }

    public function edit($id)
    {
        $this->Products->updateOrFail($id, $this->request->getData());
        return $this->responseWithoutContent();
    }

    public function delete($id)
    {
        $this->Products->deleteOrFailResponse($id);
        return $this->responseWithoutContent();
    }
}
