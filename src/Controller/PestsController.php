<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Table\PestsTable;
use SwaggerBake\Lib\Annotation\SwagQuery;
use SwaggerBake\Lib\Annotation\SwagResponseSchema;

/**
 * Pests Controller
 * @property PestsTable $Pests
 */
class PestsController extends AppController
{
    /**
     * @SwagQuery(name="name", description="find products by name.")
     * @SwagQuery(name="nested", type="string", description="get related items: dosages", example="dosages")
     * @SwagQuery(name="noPage", description="paginate data.", type="boolean")
     * @SwagResponseSchema(httpCode=200,refEntity="#/components/schemas/Pagination")
     * @return \Cake\Http\Response
     * @throws \ErrorException
     */
    public function index()
    {
        return $this->responseJson(
            $this->ExpressRequest->search(
                $this->request,
                $this->Pests
            )
        );
    }

    public function view($id)
    {
        return $this->responseJson(
            $this->Pests->getByIdOrFail($id)
        );
    }

    public function add()
    {
        $entity = $this->Pests->instanceAndValidate(
            $this->request->getData()
        );

        $this->Pests->saveOrFail($entity);
        return $this->responseJson($entity);
    }

    public function edit($id)
    {
        $this->Pests->updateOrFail($id, $this->request->getData());
        return $this->responseWithoutContent();
    }

    public function delete($id)
    {
        $this->Pests->deleteOrFailResponse($id);
        return $this->responseWithoutContent();
    }
}
