<?php
declare(strict_types=1);

namespace App\Controller;

use http\Env\Response;
use SwaggerBake\Lib\Annotation\SwagQuery;
use SwaggerBake\Lib\Annotation\SwagResponseSchema;

/**
 * Dosages Controller
 *
 * @property \App\Model\Table\DosagesTable $Dosages
 */
class DosagesController extends AppController
{
    /**
     * @SwagQuery(name="unit_type", type="string", description="find by types of dosage")
     * @SwagQuery(name="product_id", type="integer", description="find by product")
     * @SwagQuery(name="culture_id", type="integer", description="find by culture")
     * @SwagQuery(name="pest_id", type="integer", description="find by pest")
     * @SwagQuery(name="noPage", description="paginate data.", type="boolean")
     * @SwagQuery(name="nested", type="string", description="get related items: products, pests, cultures", example="cultures,products,pests")
     * @SwagResponseSchema(httpCode=200,refEntity="#/components/schemas/Pagination")
     * @return \Cake\Http\Response
     * @throws \ErrorException
     */
    public function index()
    {
        return $this->responseJson(
            $this->ExpressRequest->search(
                $this->request,
                $this->Dosages
            )
        );
    }

    public function view($id)
    {
        return $this->responseJson(
            $this->Dosages->getByIdOrFail($id)
        );
    }

    public function add()
    {
        $entity = $this->Dosages->instanceAndValidate(
            $this->request->getData()
        );

        $this->Dosages->saveOrFail($entity);
        return $this->responseJson($entity);
    }

    public function edit($id)
    {
        $this->Dosages->updateOrFail($id, $this->request->getData());
        return $this->responseWithoutContent();
    }

    public function delete($id)
    {
        $this->Dosages->deleteOrFailResponse($id);
        return $this->responseWithoutContent();
    }

    /**
     * @SwagResponseSchema(mimeType="application/pdf")
     */
    public function printPdf()
    {
        $dosages = $this->Dosages->find()
            ->contain(['Products', 'Cultures', 'Pests']);
        $this->set('dosages', $dosages);
        $this->viewBuilder()->setClassName('CakePdf.Pdf');
    }
}
