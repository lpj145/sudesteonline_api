<?php
declare(strict_types=1);

namespace App\Middleware;

use App\Exception\PersistenceException;
use App\Exception\ValidationException;
use Cake\Http\Response;
use Cake\ORM\Entity;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * PersistenceFailed middleware
 */
class PersistenceFailedMiddleware implements MiddlewareInterface
{
    /**
     * Process method.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request.
     * @param \Psr\Http\Server\RequestHandlerInterface $handler The request handler.
     * @return \Psr\Http\Message\ResponseInterface A response.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return $handler->handle($request);
        } catch (PersistenceException $exception) {
            return $this->responseWithErrors(
                $exception->getEntity(),
                $exception->getMessage()
            );
        } catch (ValidationException $exception) {
            return $this->responseWithErrors(
                $exception->getEntity(),
                'Essa requisição contém dados inválidos.'
            );
        }
    }

    protected function responseWithErrors(Entity $entity, string $message = '', $meta = null)
    {
        $result = [
            'invalids' => $entity->getInvalid(),
            'errors' => $this->transformEntityErrors($entity->getErrors()),
            'message' => $message,
        ];

        if (!is_null($meta)) {
            $result['meta'] = $meta;
        }

        return $this->responseWithJson($result, 400);
    }

    protected function responseWithJson($data, $status = 200): ResponseInterface
    {
        return (new Response())
            ->withStatus($status)
            ->withType('application/json')
            ->withStringBody(
                json_encode($data)
            );
    }

    /**
     * @param array $errors
     * @return array
     */
    private function transformEntityErrors(array $errors): array
    {
        return array_reduce(array_keys($errors), function ($oldErrors, $fieldName) use ($errors) {
            $oldErrors[$fieldName] = array_values($errors[$fieldName]);

            return $oldErrors;
        }, []);
    }
}
