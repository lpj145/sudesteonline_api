<?php
declare(strict_types=1);

namespace App\Command;

use App\Model\Table\UsersTable;
use Cake\Console\Arguments;
use Cake\Command\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

/**
 * UsersCreate command.
 */
class UsersCreateCommand extends Command
{
    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/3.0/en/console-and-shells/commands.html#defining-arguments-and-options
     *
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser = parent::buildOptionParser($parser);

        $parser->setDescription('Add user to authentication with email/password and jwt token.');

        $parser
            ->addArgument('name')
            ->addArgument('last_name')
            ->addArgument('username', [
                'help' => 'be need a valid email.'
            ])
            ->addArgument('password', [
                'help' => 'need more than 6 characters.'
            ]);


        return $parser;
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|void|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        /** @var UsersTable $table */
        $table = $this->loadModel('Users');

        $user = $table->newEntity([
            'name' => $args->getArgument('name'),
            'last_name' => $args->getArgument('last_name'),
            'username' => $args->getArgument('username'),
            'password' => $args->getArgument('password'),
        ]);

        $table->saveOrFail($user);
        $io->success(sprintf('Welcome %s to sudeste api system.', $user->username));
    }
}
