<?php
declare(strict_types=1);

namespace App\Exception;

use Cake\Datasource\EntityInterface;
use Throwable;

class PersistenceException extends \RuntimeException
{
    /**
     * @var \Cake\ORM\Entity
     */
    private $entity;

    private $defaultCode = 400;

    public function __construct(
        EntityInterface $entity,
        string $message = '',
        ?Throwable $previous = null
    ) {
        parent::__construct($message, $this->defaultCode, $previous);
        $this->entity = $entity;
    }

    public function getErrors()
    {
        return $this->entity->getErrors();
    }

    public function getInvalids()
    {
        return $this->entity->getInvalid();
    }

    /**
     * @return \Cake\ORM\Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }
}
