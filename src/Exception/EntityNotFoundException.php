<?php
declare(strict_types=1);

namespace App\Exception;

use Cake\Datasource\Exception\RecordNotFoundException;

class EntityNotFoundException extends RecordNotFoundException
{
}
