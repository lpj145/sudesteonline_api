<?php
declare(strict_types=1);

namespace App\Exception;

use Cake\ORM\Exception\PersistenceFailedException;

final class ValidationException extends PersistenceFailedException
{
}
