<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Traits\CrudTrait;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ExpressRequest\ExpressRepositoryInterface;
use ExpressRequest\Filters\SearchFilter;
use ExpressRequest\Types\FiltersCollection;

/**
 * Cultures Model
 *
 * @property \App\Model\Table\DosagesTable&\Cake\ORM\Association\HasMany $Dosages
 * @method \App\Model\Entity\Culture newEmptyEntity()
 * @method \App\Model\Entity\Culture newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Culture[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Culture get($primaryKey, $options = [])
 * @method \App\Model\Entity\Culture findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Culture patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Culture[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Culture|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Culture saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Culture[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Culture[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Culture[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Culture[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class CulturesTable extends Table implements ExpressRepositoryInterface
{
    use CrudTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('cultures');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Dosages', [
            'foreignKey' => 'culture_id',
        ]);
    }

    public function getFilterable(): FiltersCollection
    {
        return new FiltersCollection([
            new SearchFilter('name', SearchFilter::START_STRATEGY),
        ]);
    }

    public function getSelectables(): array
    {
        return [
            'id',
            'name',
        ];
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 60)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
