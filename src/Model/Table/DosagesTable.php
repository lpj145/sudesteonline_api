<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Traits\CrudTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ExpressRequest\ExpressRepositoryInterface;
use ExpressRequest\Filters\NumberFilter;
use ExpressRequest\Filters\SearchFilter;
use ExpressRequest\Types\FiltersCollection;

/**
 * Dosages Model
 *
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\BelongsTo $Products
 * @property \App\Model\Table\CulturesTable&\Cake\ORM\Association\BelongsTo $Cultures
 * @property \App\Model\Table\PestsTable&\Cake\ORM\Association\BelongsTo $Pests
 * @method \App\Model\Entity\Dosage newEmptyEntity()
 * @method \App\Model\Entity\Dosage newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Dosage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Dosage get($primaryKey, $options = [])
 * @method \App\Model\Entity\Dosage findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Dosage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Dosage[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Dosage|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Dosage saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Dosage[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Dosage[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Dosage[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Dosage[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class DosagesTable extends Table implements ExpressRepositoryInterface
{
    use CrudTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('dosages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Cultures', [
            'foreignKey' => 'culture_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Pests', [
            'foreignKey' => 'pest_id',
            'joinType' => 'INNER',
        ]);
    }

    public function getFilterable(): FiltersCollection
    {
        return new FiltersCollection([
            new SearchFilter('unit_type', SearchFilter::START_STRATEGY),
            new NumberFilter('product_id'),
            new NumberFilter('culture_id'),
            new NumberFilter('pest_id'),
        ]);
    }

    public function getSelectables(): array
    {
        return [
            'value',
            'unit_type',
            'product_id',
            'culture_id',
            'pest_id',
        ];
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('value')
            ->requirePresence('value', 'create')
            ->notEmptyString('value');

        $validator
            ->integer('product_id')
            ->requirePresence('product_id', 'create');

        $validator
            ->integer('culture_id')
            ->requirePresence('culture_id', 'create');

        $validator
            ->integer('pest_id')
            ->requirePresence('pest_id', 'create');

        $validator
            ->scalar('unit_type')
            ->maxLength('unit_type', 6)
            ->allowEmptyString('unit_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['culture_id'], 'Cultures'));
        $rules->add($rules->existsIn(['pest_id'], 'Pests'));

        return $rules;
    }
}
