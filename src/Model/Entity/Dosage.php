<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dosage Entity
 *
 * @property int $id
 * @property int $value
 * @property string|null $unit_type
 * @property int $product_id
 * @property int $culture_id
 * @property int $pest_id
 *
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Culture $culture
 * @property \App\Model\Entity\Pest $pest
 */
class Dosage extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'value' => true,
        'unit_type' => true,
        'product_id' => true,
        'culture_id' => true,
        'pest_id' => true,
        'product' => true,
        'culture' => true,
        'pest' => true,
    ];
}
