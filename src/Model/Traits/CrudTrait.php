<?php
declare(strict_types=1);

namespace App\Model\Traits;

use App\Exception\EntityNotFoundException;
use App\Exception\PersistenceException;
use App\Exception\ValidationException;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\ORM\Exception\PersistenceFailedException;
use Cake\Utility\Text;

/**
 * Trait CreateHelp
 *
 * @package Model\Traits
 * @method \Cake\Datasource\EntityInterface get($primaryKey, $options = [])
 * @method \Cake\Datasource\EntityInterface newEntity($data = null, array $options = [])
 * @method \Cake\Datasource\EntityInterface[] newEntities(array $data, array $options = [])
 * @method \Cake\Datasource\EntityInterface|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Cake\Datasource\EntityInterface saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Cake\Datasource\EntityInterface patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Cake\Datasource\EntityInterface[] patchEntities($entities, array $data, array $options = [])
 * @method \Cake\Datasource\EntityInterface findOrCreate($search, callable $callback = null, $options = [])
 */
trait CrudTrait
{
    /**
     * @param \Cake\Datasource\EntityInterface $entity
     * @param bool $generateUuid
     * @param string $messageValidation
     * @return \Cake\Datasource\EntityInterface
     */
    public function createOrFail(EntityInterface $entity, bool $generateUuid = true, string $messageValidation = '')
    {
        if ($generateUuid) {
            $this->setUuidIfNull($entity);
        }

        try {
            $this->saveOrFail($entity);
        } catch (PersistenceFailedException $exception) {
            throw new PersistenceException($entity, $messageValidation);
        }

        return $entity;
    }

    public function getByIdOrFail($id)
    {
        try {
            return $this->get($id);
        } catch (RecordNotFoundException $exception) {
            throw new EntityNotFoundException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $id
     */
    public function deleteOrFailResponse($id)
    {
        try {
            $entity = $this->get($id);
        } catch (RecordNotFoundException $exception) {
            throw new EntityNotFoundException($exception->getMessage(), $exception->getCode());
        }

        try {
            $this->deleteOrFail($entity);
        } catch (PersistenceException $exception) {
            throw new PersistenceException($exception->getEntity(), $exception->getMessage());
        }
    }

    /**
     * @param $requestData
     * @param array $options
     * @param string $messageValidation
     * @return \Cake\Datasource\EntityInterface
     */
    public function instanceAndValidate($requestData, $options = [], string $messageValidation = ''): EntityInterface
    {
        $entity = $this->newEntity(
            $requestData,
            $options
        );

        if ($entity->hasErrors()) {
            throw new ValidationException($entity, $messageValidation);
        }

        return $entity;
    }

    public function updateOrFail($primaryKey, $data, ?callable $fun = null, string $messageValidation = '')
    {
        try {
            $entity = $this->get($primaryKey);
        } catch (RecordNotFoundException $exception) {
            throw new EntityNotFoundException($exception->getMessage(), $exception->getCode());
        }

        $entity = $this->patchEntity($entity, $data);

        if (is_callable($fun)) {
            $entity = $fun($entity);
        }

        try {
            $this->saveOrFail($entity);
        } catch (PersistenceFailedException $exception) {
            throw new PersistenceException($entity, $messageValidation);
        }

        return $entity;
    }

    /**
     * @param \Cake\Datasource\EntityInterface $entity
     * @param string $propertyName
     * @return \Cake\Datasource\EntityInterface
     */
    protected function setUuidIfNull(EntityInterface $entity, string $propertyName = 'id')
    {
        if (is_null($entity->get($propertyName))) {
            $entity->set($propertyName, Text::uuid());
        }

        return $entity;
    }
}
