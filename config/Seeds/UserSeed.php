<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * User seed.
 */
class UserSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'id' => \Cake\Utility\Text::uuid(),
            'name' => 'John',
            'last_name' => 'Doe',
            'username' => 'test@email.com',
            'password' => (new \Cake\Auth\DefaultPasswordHasher())->hash('testmail')
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
