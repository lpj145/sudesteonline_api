<?php
return [
    'SwaggerBake' => [
        'prefix' => '/api',
        'yml' => '/config/swagger.yml',
        'json' => '/webroot/swagger.json',
        'webPath' => '/swagger.json',
        'hotReload' => \Cake\Core\Configure::read('debug'),
        'requestAccepts' => ['application/x-www-form-urlencoded'],
        'responseContentTypes' => ['application/json'],
        'docType' => 'swagger',
//        'namespaces' => [
//            'controllers' => ['\App\\'],
//            'tables' => ['\App\\'],
//            'entities' => ['\App\\'],
//        ]
    ]
];


