<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateProducts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('products');

        $table->addColumn('name', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 60
        ]);

        $table->addColumn('vendor_name', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 60,
        ]);

        $table->addColumn('type', 'string', [
            'default' => null,
            'null' => true,
            'limit' => 20,
        ]);

        $table->create();
    }
}
