<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateDosages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('dosages');

        $table->addColumn('value', 'integer', [
            'default' => null,
            'null' => false,
        ]);

        $table->addColumn('unit_type', 'string', [
            'default' => null,
            'null' => true,
            'limit' => 6,
        ]);

        $table->addColumn('product_id', 'integer', [
            'default' => null,
            'null' => false,
        ]);

        $table->addColumn('culture_id', 'integer', [
            'default' => null,
            'null' => false,
        ]);

        $table->addColumn('pest_id', 'integer', [
            'default' => null,
            'null' => false,
        ]);

        $table->addForeignKey(['culture_id'], 'cultures');
        $table->addForeignKey(['product_id'], 'products');
        $table->addForeignKey(['pest_id'], 'pests');

        $table->create();
    }
}
