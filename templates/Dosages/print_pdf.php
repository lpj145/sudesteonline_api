<?php
/**
 * Created by PhpStorm.
 * User: Marquinho
 * Date: 30/07/2020
 * Time: 22:31
 * @property Dosage[] $dosages
 */

use App\Model\Entity\Dosage;

?>
<h3 style="text-align: center;padding-top: 10px;">Lista Com Dosagens</h3>
<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Cultura</th>
            <th>Praga</th>
            <th>Dosagem</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dosages as $dosage): ?>
            <tr>
                <td><?= $dosage->product->name; ?></td>
                <td><?= $dosage->culture->name; ?></td>
                <td><?= $dosage->pest->name; ?></td>
                <td><?= $dosage->value; $dosage->unit_type; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
